% Header
\pdfoutput=1
\documentclass[a4paper, superscriptaddress, amsfonts, amssymb, amsmath, reprint, showkeys, nofootinbib, twoside]{revtex4-1}

\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
%\usepackage{eNumitem}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{subfigure}
\usepackage{multirow}
\usepackage[pdftex, pdftitle={Article}, pdfauthor={Author}]{hyperref} % For hyperlinks in the PDF
\usepackage{xcolor}
\usepackage{refcount}
%\usepackage{cuted}

\newcommand{\todo}[1]{\textcolor{red}{#1}}

\usepackage{amsmath}
\newcommand{\bs}{\boldsymbol}
\newcommand{\Hquad}{\hspace{0.5em}} 

\newcommand{\mc}{\mathcal}
\newcommand*\diff{\mathop{}\!\mathrm{d}}
\newcommand{\overbar}[1]{\mkern 1.5mu\overline{\mkern-1.5mu#1\mkern-1.5mu}\mkern 1.5mu}
\let\nvec\vec

\begin{document}
\title{New rank notes}

\maketitle

\section{Notes}

As we pick candidates by high SNRs (SNR$>$4). These candidates could be due to real signals or high-amplitude glitches. We want to compare our candidates against these two hypothesis. We assume there is always underlying additive Gaussian noise $\vec{n}$ in the data, the observed data can be modeled as:
  \begin{equation}
    \vec{d} =
    \begin{cases}
      \vec{s}+\vec{n}, & \text{if}\quad \mathsf{M}_s \\
      \vec{g} + \vec{n}, & \text{if} \quad \mathsf{M}_g
    \end{cases}
  \end{equation}
$\vec{d}$ is the observed data, $\mathsf{M}_s$ denotes the signal model that data comprises a signal and the noise and $\mathsf{M}_g$ denotes the alternative model that a glitch is present in the data.
  
The new ranking statistic is the odds ratio between the probably of the data from a signal and the probably of it from glitches. It can expressed as:
\begin{equation}
O = \frac{P(\vec{d}|\mathsf{M}_s)P(\mathsf{M}_s)}{P(\vec{d}|\mathsf{M}_g)P(\mathsf{M}_g)}.
\end{equation}

Instead of using the observed data streams, we use processed statistics to represent the observations: the peak SNR $\rho_I$ and the $\xi_I^2$ calculated at the peak SNR (later could include $\rho_{\text{NuLL}}$). The odds factor becomes:
\begin{equation}
O = \frac{P(\rho_I, \xi_I^2|\mathsf{M}_s)P(\mathsf{M}_s)}{P(\rho_I, \xi_I^2|\mathsf{M}_g)P(\mathsf{M}_g)}.
\end{equation}

For simplicity, we assume there is no preference on the model $\mathsf{M}_s$ or $\mathsf{M}_g$. The conditional probability can be expanded and the odds factor becomes:
\begin{equation}
O = \frac{P(\xi_I^2|\rho_I, \mathsf{M}_s) P(\rho_I|\mathsf{M}_s)} {P(\xi_I^2|\rho_I, \mathsf{M}_g) P(\rho_I|\mathsf{M}_g)}
\end{equation}

The prior probability of $\rho_I$ given signal can draw from astrophysical injections and the prior understanding of $\rho_I$ given noise can be drawn from real noise. In practice $\rho_I$ values can be high, either from close source or from we don't have preference of it to be from signal or from noise by this value alone. It is safe to assume the probability of $\rho_I$ to be $0.5$ in either signal or noise condition.

The $\rho$ is computed in discrete form as:
\begin{equation}
\begin{aligned}
\rho[j] &= \vec{d} \otimes \vec{h}[j] \\
& = \sum_{l=-m}^{m} d[l]^*\cdot h[l+j]
\end{aligned}
\end{equation}
where $\otimes$ is the cross-correlation and $\vec{h}$ is the complex template to account for the initial phase shift and normalized that its inner product equals one, i.e. $\vec{h} \otimes \vec{h}^* = 1$ (Note the pipeline normalized the template that its inner product is 2. Here we use this normalization to simplify the auto-correlation matrix $a$). As data is real, so $d^* = d$.

The $\xi_I^2$ is computed in time domain as:
\begin{equation}
\begin{aligned}
\xi_I^2 & = &  \sum_j \left| \rho[j] - \rho_I a[j]\right|^2
\end{aligned}
\end{equation}
$\rho_I$ is the observed peak SNR and $a[j]$ is the auto-correlation of the template and is expressed as:
\begin{equation}
\begin{aligned}
a[j] & = \vec{h}^* \otimes \vec{h}[j] \\
& = \sum_l h^*[l] h[l+j]
\end{aligned}
\end{equation}

  
When in hypothesis $\mathsf{M}_s$ and the template is a perfect match of the signal. The $\xi_I$ becomes:
\begin{equation}
\begin{aligned}
\xi_I^2 & = \sum_j  |\vec{n} \otimes \vec{h}[j]-  \vec{n}  \otimes \vec{h}[0] \cdot a[j] |^2.
\end{aligned}
\end{equation}

Define:
\begin{equation}
N[j] = \vec{n} \otimes \vec{h}[j]
\end{equation}
that $N[0] = \vec{n} \otimes \vec{h}[0]$.
$\xi_I^2$ can be written as:
\begin{equation}
\begin{aligned}
\xi_I^2[j] & = |N[j]-  N[0] a[j]|^2 \\
& = (N[j] - N[0] a[j]) (N[j] - N[0]a[j])^* \\
& = N[j]^2 -N[j]N[0]^*a[j]^* - N[0]a[j]N[j]^* + N[0]^2|a[j]|^2 \\
\end{aligned}
\end{equation}

Because we can express the component as:
\begin{equation}
\begin{aligned}
N[0]a[j]N[j]^* & = \vec{n}[0] \otimes \vec{h}[0] \cdot \vec{n}[j] \otimes \vec{h} [j]^* a[j] \\
 & \approx  \vec{n}[0] \otimes \vec{h}[0] \cdot \vec{n}[0] \otimes \vec{h} [0]^* \cdot \vec{h} [0] \otimes \vec{h} [j]^* \cdot a[j] \\
 & = N [0]^2 \cdot |a[j]|^2\\
 \end{aligned}
\end{equation}
Similarly we can calculate $N[j]N[0]^*a[j]^*$  with the same outcome. Therefore,
\begin{equation}
\begin{aligned}
\xi_I^2[j] & = N[j]^2 - N[0]^2|a[j]|^2.\\
\end{aligned}
\end{equation}


$N[j]$ and $N[0]$ are complex random variables. The real part and imaginary part of each are unit Gaussian distributions. $N[j]$ and $N[0]$ are correlated. For simplicity, we assume $N[j]$ ($j \neq 0$) and $N[0]$ are independent. (This assumption would not affect the expected value of the distribution but will have a larger variance than the real distribution (FIXME)). 
\begin{equation}
\begin{aligned}
\xi_I^2[j] & \sim 2 (1-|a[j]|^2)\chi^2.\\
\end{aligned}
\end{equation}
It is a scaled central $\chi^2$ distribution. 

When there is a glitch in the data instead of a signal, 
\begin{equation}
\begin{aligned}
\xi_I^2 = &  \sum_j  \left| \vec{d}[j] \otimes \vec{h}[j] - (\vec{d}[0]\otimes \vec{h}[0]) \cdot a[j]\right|^2 \\
= & \sum_j  | \left (\vec{g}[j] \otimes \vec{h}[j] - \vec{g}[0] \otimes \vec{h}[0] \cdot a[j]\right) + \\
& \left(\vec{n}[j] \otimes \vec{h}[j] - \vec{n} \otimes \vec{h}[0] \cdot a[j] \right) |^2
\end{aligned}
\end{equation}

We model $g$ as a Delta function glitch that only $g[0] \neq 0$ and $g[j]\otimes h[j] = 0$ when $j \neq 0$. Then this feature can be expressed as:


\begin{equation}
\begin{aligned}
\xi_I^2 =  \sum_{j \neq 0, j=-m}^{j=m}  & | \left ( - \rho_I \cdot a[j]\right) +  (N [j] - N[0]a[j])|^2 \\
\end{aligned}
\label{eq:chisq_g}
\end{equation}

%We simplify Eq.~\ref{eq:chisq_g} variance from the Gauss 
As we know $N[j]$ is a complex variable and both real part and imaginary part are unit Gaussians. The expectation of $\xi_I^2$ and the variance are then:
\begin{equation}
\begin{aligned}
<\xi_I^2> &=  2 \sum_j ( 1 - a[j]^2) + \rho_I^2 \sum_{j\neq 0} a[j]^2. \\
\sigma^2 &= 4 \sum_j ( 1 - a[j]^2)  + 4 \rho_I^2 \sum_{j\neq 0} a[j]^2. \\
\end{aligned}
\end{equation}
$\xi^2$ follows a non-central $\chi^2$ distribution where non-centrality parameter $\lambda = \rho_I^2  \sum_j a[j]^2 $ and degree of freedom $k = 2 \sum_j ( 1 - a[j]^2)$.

This is consistent with Eq.8.1 of Ref.~\cite{bruce2004} (Bruce2004) when calculating the $\xi^2$ with non-overlapping template divisions in the no-noise scenario: 
\begin{equation}
\begin{aligned}
<\xi^2> &=  2 (p - 1) + \kappa |\rho_I^2| \\
\sigma^2 &= 4 (p - 1) + 4 \kappa |\rho_I^2| \\
\end{aligned}
\end{equation}
where $p$ is the number of non-overlapping divisions of the time-domain template that each division contributes equally to the filtered output SNR and $\kappa$ is the maximum mismatch of the template and the signal in the data. For a template placement over the parameter space that achieves 97\% overlaps with each other template, $\kappa = 0.06$. 

The glitch is modeled as a Delta function, but in reality it is not necessary a Delta but has some span that could couple with a template in some samples. We represent this scenario with $\beta$ factor and define:
\begin{equation}
\begin{aligned}
<\xi^2> &= 2 (p - 1) + \beta (\rho_I)^2 \sum_{j\neq 0} a[j]^2. \\
\sigma ^2 &= 4 (p-1) + 4 \beta (\rho_I)^2 \sum_{j\neq 0} a[j]^2. \\
\end{aligned}
\end{equation}

So we chose {\color{red} 0.3} (FIXME: how to explain this magic number scientifically. This parameter is dependent on the noise/glitches behaviours of real data. So it is suggested that this number be tuned every time of new real data. ) for our lower bound of $\beta$:
\begin{equation}
\begin{aligned}
P(\xi_I^2|\rho_I, \mathsf{M}_s) & = \int_0^{0.06} P(\xi_I^2 | \hat{\chi^2}(\lambda, k, \beta)) d \beta \\
P(\xi_I^2|\rho_I, \mathsf{M}_n) & = \int_{0.3}^{1} P(\xi_I^2 | \hat{\chi^2}(\lambda, k, \beta )) d \beta \\
\end{aligned}
\end{equation}
where $\hat{\chi^2}$ is the $\chi^2$ approximation (see Appendix).

Special treatment when $\xi^2 < 1$ we set it to be $1$. (FIXME: explain) 

The prior probability 
\begin{equation}
\frac{P(\rho_I|\mathsf{M}_s)}{P(\rho_I|\mathsf{M}_g)} = \exp{(\rho_I)^2/2}
\end{equation}


\iffalse
$h$ is normalized. When there is a mismatch between the template $h$ and the signal $s$, we define
\begin{equation}
\beta = \vec{h}\otimes\vec{s}[0].
\end{equation}
The $\xi_I^2$ becomes:
\begin{equation}
\begin{aligned}
\xi_I^2 & = &  \sum_j | \beta \vec{d}\otimes \vec{h}(t_j) + \vec{n}(t_j)- \\
& & (\beta+ \vec{n}[0])*(\vec{h}\otimes \vec{h}(t_j))|^2) 
\end{aligned}
\end{equation}
\fi
\section{Appendix}
\subsection{Convolution Theorem}
From the Convolution theorem:
\begin{equation}
\begin{aligned}
& \mathcal{F} (\int a(t) b(\tau-t)dt) \\
&= \int \int a(t) b(\tau-t) dt e^{-i2\pi \tau f} d\tau \\
&= \int a(t) e^{-i2\pi tf} dt \int b(\tau -t) e^{-i2\pi (\tau - t)f} d(\tau -t) \\
& =\mathcal{F}(a(t)) \mathcal{F} (b(t))
\end{aligned}
\end{equation}
replace $a(t)$ with a complex conjugate function $h^*(t)$, and $b(t)$ with the inverse $h(t)$, we can obtain:
\begin{equation}
\begin{aligned}
& \mathcal{F} (\int h^*(t) h(t+\tau)dt) \\
&= \int \int h^*(t) h(t+\tau) dt e^{-i2\pi \tau f} d\tau \\
&= \int h^*(t) e^{i2\pi tf} dt \int h(t + \tau) e^{-i2\pi (t+\tau)f} d(t+\tau) \\
&= \left( \int h(t) e^{-i2\pi tf} dt\right)^* \int h(t + \tau) e^{-i2\pi (t+\tau)f} d(t+\tau) \\
& = \mathcal{F}(h(t))^* \mathcal{F} (h(t))
\end{aligned}
\end{equation}

So the cross-correlation can be calculated as:
\begin{equation}
\mathcal{F} (\vec{h}^* \otimes \vec{h}[j]) = \mathcal{F}(h)^* \mathcal{F} (h)
\end{equation}

\subsection{Overflow with $\chi^2$ distribution}

If we use $\chi^2$ distribution for our $\xi^2$ statistic, it can easily get overflow. E.g. when the degree of freedom $k = 2 \sum_j (1-a[j]^2) = 681$ the first template of a BNS bank, the non-centrality parameter $\lambda = \rho_I^2  \sum_j a[j]^2 = 2200$ when $\rho=15$, the distribution of this non-central $\chi^2$ is:
\includegraphics[width=6cm, height=5cm]{ncx_example}

Any reduced $\chi^2$ value below 0.48 will cause machine overflow and be assigned to 0. When the SNR is larger than 15, the reduced value needs to be larger to be overflow.

The Gaussian approximation is supposed to agree with the $\chi^2$ distribution the high prob region well
based on:
From Bruce2004: ``In the neighborhood of the maximum, for values of
the non-centrality parameter $\lambda = \kappa  |\rho_I^2| $ significantly larger than
$2(p-1)$, the non-central $\chi^2$ distribution is approximately
a Gaussian of width $\sigma$, centered about the mean value 
$2(p-1) + \lambda$." 

and based on the ``generalized central limit theorem": the sum of a number of independent and identically distributed (i.i.d.) random variables with finite non-zero variances will tend to a normal distribution as the number of variables grows. (Our $p$ is normally 351)

This Gaussian distribution is shown:
\includegraphics[width=6cm, height=5cm]{norm_example}

Overlapping the two distributions, it is clear the Gaussian agrees well in the high probability region, but not match well in the low prob region (FIXME, consider a linear approximation in the low prob region of this log scale!). 
\includegraphics[width=6cm, height=5cm]{ncx_and_norm_log}


\end{document}

